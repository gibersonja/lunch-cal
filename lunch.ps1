$outlook    = new-object -com Outlook.Application
$calendar   = $Outlook.session.GetDefaultFolder(9)
$start_time = Get-Date -UFormat "%m/%d/%Y %R"
$motd       = Invoke-RestMethod -Uri "https://api.quotable.io/random"
$stop_time  = (Get-Date $start_time).AddDays(+0.041666)

$appt          = $calendar.Items.Add(1) # == olAppointmentItem
$appt.Start    = $start_time
$appt.End      = $stop_time
$appt.Subject  = "----- Lunch -----"
$appt.Location = ""
$appt.Body     = $motd.content + "`r`n`r`n" + $motd.author

$appt.Save()
